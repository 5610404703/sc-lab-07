package View;

import javax.swing.JButton;

public class ButtonCinema extends JButton {

	private static final long serialVersionUID = 1L;
	private int check;
	private int row;
	private int column;
	
	public ButtonCinema(String str, int check, int row, int column) {
		super(str);
		this.check = check;
		this.row = row;
		this.column = column;
	}
	
	public int getCheck() {
		return check;
	}
	
	public void setCheck(int x) {
		check = x;
	}
	
	public int getRow() { 
		return row;
	}
	
	public int getColumn() {
		return column;
	}

	
}
