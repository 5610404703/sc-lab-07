package View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import Controller.TheaterManagement;

public class TheaterSystemGUI{
	private TheaterManagement theaterManagement;
	private JFrame frame;
	private JPanel topPanel;
	private JPanel centerPanel,centerPanel2;
	private JPanel downPanel;
	private JComboBox<String> comboBoxPrice;
	private JComboBox<String> comboBoxDay;
	private JComboBox<String> comboBoxTime;

	private JLabel total;
	private int[][] ticketPrice;
	private ButtonCinema[][] seats = new ButtonCinema[15][20];

	public TheaterSystemGUI() {
		theaterManagement = new TheaterManagement();
	}
	
	public void createFrame() {
		frame = new JFrame();
		frame.setTitle("Theater");
		frame.setSize(1500, 1000);
		frame.setLocation(150, 40);
		frame.setLayout(new BorderLayout());
		
		
		topPanel = new JPanel();
		topPanel.setBackground(new Color(148, 187, 250));
		centerPanel = new JPanel();
		centerPanel2 = new JPanel();
		centerPanel.setLayout(new GridLayout(15, 20, 5, 5));
		centerPanel2.setLayout(new GridLayout(15, 20, 5, 5));
		downPanel = new JPanel();
		downPanel.setLayout(new GridLayout(1, 5));

		frame.add(topPanel, BorderLayout.NORTH);
		frame.add(centerPanel, BorderLayout.CENTER);
		frame.add(downPanel, BorderLayout.SOUTH);
		frame.setVisible(false);
		JLabel labelScreen = new JLabel("SCREEN");
		topPanel.add(labelScreen);
			
		centerPanel.setBorder(new TitledBorder(UIManager.getBorder("TitleBorder.border"), "Seat", TitledBorder.LEADING, TitledBorder.TOP));
		centerPanel2.setBorder(new TitledBorder(UIManager.getBorder("TitleBorder.border"), "Seat", TitledBorder.LEADING, TitledBorder.TOP));
		ticketPrice = theaterManagement.getSeatPrice();

		for (int i = 0; i < ticketPrice.length; i++) {
			for (int j = 0; j < ticketPrice[i].length; j++) {
				ButtonCinema a;
				if (ticketPrice[i][j] == 0) {
					a = new ButtonCinema("", 0, i, j);
					a.setBackground(new Color(249, 159, 159));
					a.setEnabled(false);
					createSeats(a, i, j);
				} 
				else {
					a = new ButtonCinema("" + ticketPrice[i][j], 0, i, j);
					
					a.addActionListener(new ActionListener() {

						@Override
						public void actionPerformed(ActionEvent e) {
							if (a.getCheck() == 0) {
								a.setBackground(new Color(248, 40, 119));
								theaterManagement.addPrice(Integer.parseInt(a.getText()));
								total.setText("Total : " + theaterManagement.getTotal() + " Baht.");
								a.setCheck(1);
							} else {
								a.setBackground(null);
								theaterManagement.decreasePrice(Integer.parseInt(a.getText()));
								total.setText("Total : " + theaterManagement.getTotal() + " Baht.");
								a.setCheck(0);
							}
						}
					});
					createSeats(a, i, j);
					setColorSeat(ticketPrice[i][j], i, j);
				}
				centerPanel.add(a);
			}
		}
		JLabel labelShowTime = new JLabel("ShowTime : ");
		JPanel downLeftPanel = new JPanel();
		JPanel downCenterPanel = new JPanel();
		JPanel downRightPanel = new JPanel();

		downLeftPanel.setBorder(new TitledBorder(UIManager.getBorder("TitleBorder.border"), "Select day", TitledBorder.LEADING, TitledBorder.TOP));
		comboBoxDay = new JComboBox<String>();
		comboBoxDay.addItem("Weekday");
		comboBoxDay.addItem("Holiday");
		
		comboBoxTime = new JComboBox<String>();
		comboBoxTime.addItem("11.00 - 13.30");
		comboBoxTime.addItem("16.00 - 18.30");
			
		JButton buttonOKDay = new JButton("OK");
		JButton buttonOKTime = new JButton("OK");
		
		downLeftPanel.add(comboBoxDay);	
		downLeftPanel.add(buttonOKDay);
		downLeftPanel.add(comboBoxTime);
		downLeftPanel.add(buttonOKTime);
		downPanel.add(downLeftPanel);
		buttonOKDay.addActionListener(new ActionListener() {
					
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(comboBoxDay.getSelectedItem().toString().equals("Weekday")){
					comboBoxTime.removeItem("20.00 - 22.30");
				}
				else if(comboBoxDay.getSelectedItem().toString().equals("Holiday")){
					comboBoxTime.addItem("20.00 - 22.30");
				}
			}
		});
		

		buttonOKTime.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(comboBoxDay.getSelectedItem().toString().equals("Weekday")){
					if(comboBoxTime.getSelectedItem().toString().equals("11.00 - 13.30")){
						
						for (int i = 0; i < ticketPrice.length; i++) {
							for (int j = 0; j < ticketPrice[i].length; j++) {
								ButtonCinema weekday1;
								if (ticketPrice[i][j] == 0) {
									weekday1 = new ButtonCinema("", 0, i, j);
									weekday1.setBackground(new Color(249, 159, 159));
									weekday1.setEnabled(false);
									createSeats(weekday1, i, j);
								} 
								else {
									weekday1= new ButtonCinema("" + ticketPrice[i][j], 0, i, j);
									
									weekday1.addActionListener(new ActionListener() {

										@Override
										public void actionPerformed(ActionEvent e) {
											if (weekday1.getCheck() == 0) {
												weekday1.setBackground(new Color(248, 40, 119));
												theaterManagement.addPrice(Integer.parseInt(weekday1.getText()));
												total.setText("Total : " + theaterManagement.getTotal() + " Baht.");
												weekday1.setCheck(1);
											} 
											else {
												weekday1.setBackground(null);
												theaterManagement.decreasePrice(Integer.parseInt(weekday1.getText()));
												total.setText("Total : " + theaterManagement.getTotal() + " Baht.");
												weekday1.setCheck(0);
											}
										}
									});
									createSeats(weekday1, i, j);
									setColorSeat(ticketPrice[i][j], i, j);
								}
								frame.pack();
								frame.remove(centerPanel2);
								frame.pack();
								frame.add(centerPanel);
								frame.pack();
								centerPanel.add(weekday1);
								frame.pack();
								frame.setVisible(true);
							}
						}						
					}
					else if(comboBoxTime.getSelectedItem().toString().equals("16.00 - 18.30")){					
						for (int i = 0; i < ticketPrice.length; i++) {
							for (int j = 0; j < ticketPrice[i].length; j++) {
								ButtonCinema weekday2;
								if (ticketPrice[i][j] == 0) {
									weekday2 = new ButtonCinema("", 0, i, j);
									weekday2.setBackground(new Color(249, 159, 159));
									weekday2.setEnabled(false);
									createSeats(weekday2, i, j);
								} 
								else {
									weekday2 = new ButtonCinema("" + ticketPrice[i][j], 0, i, j);
									
									weekday2.addActionListener(new ActionListener() {

										@Override
										public void actionPerformed(ActionEvent e) {
											if (weekday2.getCheck() == 0) {
												weekday2.setBackground(new Color(248, 40, 119));
												theaterManagement.addPrice(Integer.parseInt(weekday2.getText()));
												total.setText("Total : " + theaterManagement.getTotal() + " Baht.");
												weekday2.setCheck(1);
											} else {
												weekday2.setBackground(null);
												theaterManagement.decreasePrice(Integer.parseInt(weekday2.getText()));
												total.setText("Total : " + theaterManagement.getTotal() + " Baht.");
												weekday2.setCheck(0);
											}
										}
									});
					
									createSeats(weekday2, i, j);
									setColorSeat(ticketPrice[i][j], i, j);
								}
								frame.pack();
								frame.remove(centerPanel);
								frame.pack();
								frame.add(centerPanel2);
								frame.pack();
								centerPanel2.add(weekday2);
								frame.pack();
								frame.setVisible(true);
								
							}
						}
					}
					
				}
				else{
					
					if(comboBoxTime.getSelectedItem().toString().equals("11.00 - 13.30")){
						for (int i = 0; i < ticketPrice.length; i++) {
							for (int j = 0; j < ticketPrice[i].length; j++) {
								ButtonCinema holiday1;
								if (ticketPrice[i][j] == 0) {
									holiday1 = new ButtonCinema("", 0, i, j);
									holiday1.setBackground(new Color(249, 159, 159));
									holiday1.setEnabled(false);
									createSeats(holiday1, i, j);
								} 
								else {
									holiday1 = new ButtonCinema("" + ticketPrice[i][j], 0, i, j);
									
									holiday1.addActionListener(new ActionListener() {

										@Override
										public void actionPerformed(ActionEvent e) {
											if (holiday1.getCheck() == 0) {
												holiday1.setBackground(new Color(248, 40, 119));
												theaterManagement.addPrice(Integer.parseInt(holiday1.getText()));
												total.setText("Total : " + theaterManagement.getTotal() + " Baht.");
												holiday1.setCheck(1);
											} else {
												holiday1.setBackground(null);
												theaterManagement.decreasePrice(Integer.parseInt(holiday1.getText()));
												total.setText("Total : " + theaterManagement.getTotal() + " Baht.");
												holiday1.setCheck(0);
											}
										}
									});
					
									createSeats(holiday1, i, j);
									setColorSeat(ticketPrice[i][j], i, j);
								}
								frame.pack();
								frame.remove(centerPanel);
								frame.pack();
								frame.add(centerPanel2);
								frame.pack();
								centerPanel2.add(holiday1);
								frame.pack();
								frame.setVisible(true);
								
							}
						
					}
					
				
						
					}
					else if(comboBoxTime.getSelectedItem().toString().equals("16.00 - 18.30")){

							for (int i = 0; i < ticketPrice.length; i++) {
								for (int j = 0; j < ticketPrice[i].length; j++) {
									ButtonCinema holiday2;
									if (ticketPrice[i][j] == 0) {
										holiday2 = new ButtonCinema("", 0, i, j);
										holiday2.setBackground(new Color(249, 159, 159));
										holiday2.setEnabled(false);
										createSeats(holiday2, i, j);
									} 
									else {
										holiday2 = new ButtonCinema("" + ticketPrice[i][j], 0, i, j);
										
										holiday2.addActionListener(new ActionListener() {

											@Override
											public void actionPerformed(ActionEvent e) {
												if (holiday2.getCheck() == 0) {
													holiday2.setBackground(new Color(248, 40, 119));
													theaterManagement.addPrice(Integer.parseInt(holiday2.getText()));
													total.setText("Total : " + theaterManagement.getTotal() + " Baht.");
													holiday2.setCheck(1);
												} else {
													holiday2.setBackground(null);
													theaterManagement.decreasePrice(Integer.parseInt(holiday2.getText()));
													total.setText("Total : " + theaterManagement.getTotal() + " Baht.");
													holiday2.setCheck(0);
												}
											}
										});
						
										createSeats(holiday2, i, j);
										setColorSeat(ticketPrice[i][j], i, j);
									}
									frame.pack();
									frame.remove(centerPanel);
									frame.pack();
									frame.add(centerPanel2);
									frame.pack();
									centerPanel2.add(holiday2);
									frame.pack();
									frame.setVisible(true);
									
								}
							
						}
						
						
						
					}
					else if(comboBoxTime.getSelectedItem().toString().equals("20.00 - 22.30")){
						for (int i = 0; i < ticketPrice.length; i++) {
							for (int j = 0; j < ticketPrice[i].length; j++) {
								ButtonCinema holiday3;
								if (ticketPrice[i][j] == 0) {
									holiday3 = new ButtonCinema("", 0, i, j);
									holiday3.setBackground(new Color(249, 159, 159));
									holiday3.setEnabled(false);
									createSeats(holiday3, i, j);
								} 
								else {
									holiday3 = new ButtonCinema("" + ticketPrice[i][j], 0, i, j);
									
									holiday3.addActionListener(new ActionListener() {

										@Override
										public void actionPerformed(ActionEvent e) {
											if (holiday3.getCheck() == 0) {
												holiday3.setBackground(new Color(248, 40, 119));
												theaterManagement.addPrice(Integer.parseInt(holiday3.getText()));
												total.setText("Total : " + theaterManagement.getTotal() + " Baht.");
												holiday3.setCheck(1);
											} else {
												holiday3.setBackground(null);
												theaterManagement.decreasePrice(Integer.parseInt(holiday3.getText()));
												total.setText("Total : " + theaterManagement.getTotal() + " Baht.");
												holiday3.setCheck(0);
											}
										}
									});
					
									createSeats(holiday3, i, j);
									setColorSeat(ticketPrice[i][j], i, j);
								}
								frame.pack();
								frame.remove(centerPanel);
								frame.pack();
								frame.add(centerPanel2);
								frame.pack();
								centerPanel2.add(holiday3);
								frame.pack();
								frame.setVisible(true);
								
							}
						
					}
					
					
					
				
						
						
						
					}
					
				}
				
			}
		});
		
		
		
		
		
		downCenterPanel.setBorder(new TitledBorder(UIManager.getBorder("TitleBorder.border"), "Select showtime and search seats by price/seats", TitledBorder.LEADING, TitledBorder.TOP));
		
		JLabel labelSeatPrice = new JLabel("Seat Price : ");
		comboBoxPrice = new JComboBox<String>();
		comboBoxPrice.addItem("All");
		comboBoxPrice.addItem("10");
		comboBoxPrice.addItem("20");
		comboBoxPrice.addItem("30");
		comboBoxPrice.addItem("40");
		comboBoxPrice.addItem("50");
		
		JButton buttonSearch = new JButton("Search");
		
		
		downCenterPanel.add(labelShowTime);
		downCenterPanel.add(labelSeatPrice);
		downCenterPanel.add(comboBoxPrice);
		downCenterPanel.add(buttonSearch);
		
		downPanel.add(downCenterPanel);
		
		buttonSearch.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				for (int i = 0; i < 15; i++) {
					for (int j = 0; j < 20; j++) {
						if (comboBoxPrice.getSelectedItem().toString().equals("")) {
							if (ticketPrice[i][j] == 0) {
								seats[i][j].setEnabled(false);
							}
							else {
								seats[i][j].setEnabled(true);
								
							}
						}
						else {
							if (seats[i][j].getText().equals("") || seats[i][j].getText().equals("---")) {
							}
							else if (seats[i][j].getCheck() == 1) {	
								setColorSeat(ticketPrice[i][j], i, j);
							}
							else if(comboBoxPrice.getSelectedItem().toString().equals("All")){
								seats[i][j].setEnabled(true);
								setColorSeat(ticketPrice[i][j], i, j);
							}
							else if (seats[i][j].getText().equals(comboBoxPrice.getSelectedItem().toString())) {
									seats[i][j].setEnabled(true);
									setColorSeat(ticketPrice[i][j], i, j);
							}
							else {
								seats[i][j].setBackground(null);
								seats[i][j].setEnabled(false);
							}
						}
					}
				}
			}
		});
		
		JButton buttonBuy = new JButton("Buy");
		downRightPanel.setBorder(new TitledBorder(UIManager.getBorder("TitleBorder.border"), "Price", TitledBorder.LEADING, TitledBorder.TOP));
		total = new JLabel("Total : " + theaterManagement.getTotal() + " Baht.");
		
		downRightPanel.add(total);
		downRightPanel.add(buttonBuy);
	
		downPanel.add(downRightPanel);
		
		buttonBuy.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				for (int i = 0; i < 15; i++) {
					for (int j = 0; j < 20; j++) {
						if (seats[i][j].getCheck() == 1) {
							theaterManagement.setDataSeatPrice(i, j, 0);
							seats[i][j].setText("---");
							seats[i][j].setCheck(3);
							seats[i][j].setEnabled(false);
							theaterManagement.clearTotal();
							total.setText("Total : " + theaterManagement.getTotal() + " Baht.");
						}
					}
				}
			}
		});

		frame.pack();
		frame.setVisible(true);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
				
	public void createSeats(ButtonCinema a, int i, int j) {
		seats[i][j] = a;
	}
	
	public void setColorSeat(int price,int i, int j){
		if (price==10){
			seats[i][j].setBackground(new Color(148, 251, 55));
		}
		else if(price==20){
			seats[i][j].setBackground(new Color(215, 246, 60));
		}
		else if(price==30){
			seats[i][j].setBackground(new Color(78, 228, 139));
		}
		else if(price==40){
			seats[i][j].setBackground(new Color(212, 140, 207));
		}
		else if(price==50){
			seats[i][j].setBackground(new Color(178, 139, 255));
		}
	}
}
