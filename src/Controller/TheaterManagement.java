package Controller;

import Model.DataSeatPrice;

public class TheaterManagement {
	private DataSeatPrice dataSeatPrice;
	private double total = 0;
	
	public TheaterManagement() {
		dataSeatPrice = new DataSeatPrice();
	}
	
	public int[][] getSeatPrice() {
		int a[][] = dataSeatPrice.getDataSeatPrice();
		int x[][] = new int[15][20];
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[i].length; j++) {
				x[i][j] = a[i][j];
			}
		}
		return x;
	}
	
	public void setDataSeatPrice(int i, int j, int price) {
		dataSeatPrice.setDataSeatPrice(i, j, price);;
	}
	
	public void addPrice(int price) {
		total += price;
	}
	
	public void decreasePrice(int price) {
		total -= price;
	}
	
	public double getTotal() {
		return total;
	}
	
	public void clearTotal() {
		total = 0;
	}

}
